# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require asyn,       4.33.0
# require autosave,   5.9.0
require calc,       3.7.1
# require iocStats,   ae5d083
# require pvaSrv      0.12.0  # v7 ?
# require recsync,    1.3.0
require sequencer,  2.2.6
require stream,     2.8.8
# -----------------------------------------------------------------------------
require asynfile,   0.0.2

# -----------------------------------------------------------------------------
# EPICS siteApps
# -----------------------------------------------------------------------------
require lakeshore224, 0.0.3


# -----------------------------------------------------------------------------
# Utgard-Lab
# -----------------------------------------------------------------------------
#@field PORTNAME
#@type STRING
#The port name. Should be unique within an IOC.
#epicsEnvSet(PORTNAME, "PortA")
#@field IPADDR
#@type STRING
#IP or hostname of the TCP endpoint.
epicsEnvSet(IPADDR,   "10.4.3.125")
#@field IPPORT
#@type INTEGER
#Port number for the TCP endpoint.
epicsEnvSet(IPPORT,   "7777")
#@field PREFIX
#@type STRING
#Prefix for EPICS PVs.
epicsEnvSet(PREFIX,   "UTG-SEE-FLUC:Tmt-LS224-01")
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(lakeshore224_DIR)db")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
iocshLoad("$(lakeshore224_DIR)lakeshore224.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")


# -----------------------------------------------------------------------------
# SNL callback
# -----------------------------------------------------------------------------
# Not yet put this in the substitution file...Can it be put in substitutions?
# Maybe good to have it here with teh SNL stuff..
# P for name and CHANNEL, setCHANNEL, sigCHANNEL for different numbering ...
# -----------------------------------------------------------------------------
# TO BE FIXED... should be configured for inputs (A, B, C1–C5, D1–D5)
# -----------------------------------------------------------------------------
#seq callback, "name=1, P=$(PREFIX), sigCHANNEL=0, SIGNAL=KRDG, SETPOINT=SETP_S, setCHANNEL=1, CHANNEL=1"
#seq callback, "name=2, P=$(PREFIX), sigCHANNEL=1, SIGNAL=KRDG, SETPOINT=SETP_S, setCHANNEL=2, CHANNEL=2"
#seq callback, "name=3, P=$(PREFIX), sigCHANNEL=2, SIGNAL=KRDG, SETPOINT=SETP_S, setCHANNEL=3, CHANNEL=3"
#seq callback, "name=4, P=$(PREFIX), sigCHANNEL=3, SIGNAL=KRDG, SETPOINT=SETP_S, setCHANNEL=4, CHANNEL=4"


# -----------------------------------------------------------------------------
# SNL curve_install
# -----------------------------------------------------------------------------
seq install_curve, "P=$(PREFIX), CurvePrefix=File"


# -----------------------------------------------------------------------------
# Add trace...
# -----------------------------------------------------------------------------
# asynSetTraceMask($(PREFIX), -1, 0xF)
# asynSetTraceIOMask($(PREFIX), -1, 0x2)

iocInit
#startPVAServer
