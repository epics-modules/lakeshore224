#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require(lakeshore224)

# -----------------------------------------------------------------------------
# E04-Labs
# -----------------------------------------------------------------------------
#@field PORTNAME
#@type STRING
#The port name. Should be unique within an IOC.
#epicsEnvSet(PORTNAME, "PortA")
#@field IPADDR
#@type STRING
#IP or hostname of the TCP endpoint.
epicsEnvSet(IPADDR,   "172.30.32.12")
#@field IPPORT
#@type INTEGER
#Port number for the TCP endpoint.
epicsEnvSet(IPPORT,   "7777")
#@field PREFIX
#@type STRING
#Prefix for EPICS PVs.
epicsEnvSet(PREFIX,   "DEV:Tmt-LS224-01")
# 
epicsEnvSet(LOCATION, "DEV: $(IPADDR):$(IPPORT)")
# 
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(lakeshore224_DIR)db")
#
# -----------------------------------------------------------------------------
# E3 Common databases
# -----------------------------------------------------------------------------
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
iocshLoad("$(lakeshore224_DIR)lakeshore224.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")

# -----------------------------------------------------------------------------
# SNL curve_install
# -----------------------------------------------------------------------------
seq install_curve, "P=$(PREFIX), CurvePrefix=File"

# -----------------------------------------------------------------------------
# Add trace...
# -----------------------------------------------------------------------------
# asynSetTraceMask($(PREFIX), -1, 0xF)
# asynSetTraceIOMask($(PREFIX), -1, 0x2)
#
iocInit()
